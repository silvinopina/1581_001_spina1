import java.util.Arrays;
/*
* Dynamic Array 
*
*author Silvino Pina
*version 11/1/17
*
*/

public class DynamicArray{
	private String[] array;
	private int nextIndex;
	private int numberOfElements;

	public DynamicArray(){
		this(10);
		//this.array = new String[10];
		//this.nextIndex = 0;
		//this.numberOfElements = 0;
	}//end no arguments

	public DynamicArray(int size){
		this.array = new String[size];
		this.nextIndex = 0;
		this.numberOfElements = 0;
	}//end of one argument constructor

	public DynamicArray(String [] initialStrings){
		this.array = Arrays.copyOf(initialStrings,(initialStrings.length *3)/2);
		this.nextIndex = initialStrings.length;
		this.numberOfElements = initialStrings.length;
	}//end of overLoaded one argument constructor

	public void add(String stringToAdd){
		//check for space
		if(this.array.length == this.numberOfElements){
			this.resizeArray();
		}

		this.array[this.nextIndex] = stringToAdd;
		this.nextIndex = this.nextIndex + 1;
		this.numberOfElements++;
	}// end add


	private void resizeArray(){
		String[] newArray = new String[(this.array.length*3)/2];

		for (int i = 0; i < this.array.length; i++){
			newArray[i] = this.array[i];
		}
		this.array = newArray;

	}//end resize array


	public String remove(int removeIndex){
		String removedString;
		removedString = this.array[removeIndex];
		String[] newArray = new String[this.array.length - 1];
		for (int i = removeIndex; i < this.array.length; i++){
			newArray[i] = this.array[i+1];
		}
		this.array = newArray;
		return removedString;
	}//end remove

	public boolean isEmpty(){
		boolean empty;
		if(numberOfElements == 0){
			empty = true;
		}

		else{
			empty = false;
		}
		return empty;
	}//end isEmpty

	public int sizeOf(){
		return numberOfElements;
	}

	public String get(int getIndex){
		String getString;
		getString = this.array[getIndex];
		return getString;
	}



}// end class
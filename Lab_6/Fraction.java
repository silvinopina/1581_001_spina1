/**
* My first object
*
*author Silvino
*version 10-25-17
*/

public class Fraction{
	private int numerator;
	private int demoninator;

	//constructor
	public Fraction(int numerator, int demoninator){
		this.numerator = numerator;
		this.demoninator = demoninator;
	}//end constructor

	//addition method
	public Fraction add(Fraction otherFraction){
		int commonDenom = this.demoninator * otherFraction.demoninator;
		int number1 = this.numerator * otherFraction.demoninator;
		int number2 = otherFraction.numerator * this.demoninator;
		int newNum = number1 + number2;
		return new Fraction(newNum, commonDenom);
	}//end addition method

	//subtraction method
	public Fraction sub(Fraction otherFraction){
		int commonDenom = this.demoninator * otherFraction.demoninator;
		int number1 = this.numerator * otherFraction.demoninator;
		int number2 = otherFraction.numerator * this.demoninator;
		int newNum = number1 - number2;
		return new Fraction(newNum, commonDenom);
	}//end subtractiond method

	//multiplication method
	public Fraction multiply(Fraction otherFraction){
		int Demon2 = this.demoninator * otherFraction.demoninator;
		int Num2 = this.numerator * otherFraction.numerator;
		return new Fraction(Num2, Demon2);
	}// end multiplication method

	public Fraction divide(Fraction otherFraction){
		int newDemon = otherFraction.numerator;
		int newNum = otherFraction.demoninator;
		int Demon2 = this.demoninator * newDemon;
		int Num2 = this.numerator * newNum;
		return new Fraction(Num2, Demon2);
	}
	@Override
	public String toString(){
		return this.numerator + "/" + this.demoninator;
	}
	
}//end class
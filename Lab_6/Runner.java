/**
*Runner for Fraction class
*
*author Silvino
*version 10-25-17
*/

public class Runner{
	public static void main(String[] args){
		Fraction frac1 = new Fraction(1, 2);
		Fraction frac2 = new Fraction(3, 4);

		System.out.println("Fraction 1: " + frac1.toString());
		System.out.println("Fraction 2: " + frac2.toString());

		Fraction sum = frac1.add(frac2);
		System.out.println("\nSum: " + sum);

		Fraction diff = frac1.sub(frac2);
		System.out.println("\nDiff:" + diff);

		Fraction product = frac1.multiply(frac2);
		System.out.println("\nProduct: " + product);

		Fraction quiotent = frac1.divide(frac2);
		System.out.println("\nQuotient: " + quiotent);
	}//end main
}//end class
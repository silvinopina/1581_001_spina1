import java.util.Scanner;
/**	
 * @author Silvino Pina	
 * @version 9/13/17
 *Lab3
 */

public class NumberValidator{
	public static void main(String[] args){
		//Create Scanner instamce to get inout from command line
		Scanner input = new Scanner(System.in);

		int userNumber;

		System.out.println("Enter a number between 0 and 10.");
		userNumber = input.nextInt();

		if (userNumber > 0 && userNumber < 10){
			System.out.println("Valid Number");
		}

		else {
			System.out.println("Invaild Number");
		}

		// asks for int divisible by 2 or 3
		System.out.println("Enter am int that is divisible by 2 or 3");
		userNumber = input.nextInt();

		if (((userNumber % 2) == 0) || ((userNumber%3)== 0)){
			System.out.println("Valid Number");
		}

		else {
			System.out.println("Invaild Number");
		}

		// asks for a number that is negative and even or positive and odd.
		System.out.println("Enter a number that is negative and even or positive and odd.");
		userNumber = input.nextInt();

		if (((userNumber < 0) &&((userNumber % 2) == 0)) ||
			((userNumber > 0) && ((userNumber % 2) == 1))){
				System.out.println("Valid Number");
		}

		else {
			System.out.println("Invalid Number");
		}

		//Number divisble by 2 or 5 but not noth 

		System.out.println("Enter number divisble by 2 or 5 but not both");
		userNumber = input.nextInt();

		if((((userNumber % 2) == 0) || ((userNumber % 5) == 0)) && !((userNumber % 10) == 0)){
			System.out.println("Valid Number");
		}

		else {
			System.out.println("Invalid Number");
		}

		// eneter a positve odd number
		System.out.println("Enter a positive odd number");
		userNumber = input.nextInt();

		if (!((userNumber%2)==0)&& !(userNumber <= 0)){
			System.out.println("Valid Number");

		}

		else {
			System.out.println("Invalid Number");

		}


	}//end main
}//end class
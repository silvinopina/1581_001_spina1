import java.util.Scanner;
/**	
 * @author Silvino Pina	
 * @version 9/13/17
 *Lab3
 */

public class MonthGetter{
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		int month;
		do{
			System.out.println("What month is it (1-12)?");
			month = input.nextInt();
		}	while ( !(month >= 1 && month <= 12));
		System.out.println(month + " is a month. ");
	}//end maim
}//end class
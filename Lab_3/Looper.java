import java.util.Scanner;
/**	
 * @author Silvino Pina	
 * @version 9/13/17
 * Lab3
 */

public class Looper{
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		int iterations;
		System.out.println("How many iterations?");
		iterations = input.nextInt();

		for (int counter = 0; counter < iterations; counter++){
			System.out.println("Counter = " + counter);
		}// end for
	}// end main
	}// end class
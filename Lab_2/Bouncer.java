import java.util.Scanner;
/**	
 * A brief description of what this file's code does.
 *
 * @author Silvino Pina	
 * @version 8/30/17
 */

public class Bouncer{
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		int numberInLine;
		
		System.out.println("How many people are in line?");
		numberInLine=input.nextInt();

		while(numberInLine > 0){

			int age;
			System.out.println("I need to see some ID, please.");
			age=input.nextInt();
			
			if(age<18){
				System.out.println("Sorry Kid, you're not old enough.");
			}

			else if (age>=18 && age<21){
				System.out.println("Come on in, but NO DRINKS!");
			}

			else {
				System.out.println("Help yourself to booze!");
			}

			System.out.println("Nonetheless, have a good night!");
			numberInLine--;
		}

		System.out.println("Well, I guess I'll go take a nap then!");
	}
}
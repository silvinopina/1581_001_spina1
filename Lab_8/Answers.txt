1. What is the equals method inherited from Object comparing?
Its comparing the memory 

2. What is a more natural way to think of equality of two Point2D instances?
We want to compare the actual values of the coordinates

3. What is the toString method inherited from Object returning?
It is returning the memory address

4. What is a more natural way to represent a Point2D instance as a String?
To print out how we would write as if we were in math


5. What is the String output of this method call on the upcast point? What does this imply about overridden methods?
It will call Point2D to string. It still retains the characteristics of the earlier object


6. Why do subclass constructor calls work the way they do? Come up with a plausible reason for the hierarchical superclass constructor calls. (I’m not worried about a correct response, just a thoughtful one.)
The take in the values and create and object based on the information give. the object created is based of the information given.


7. Did the attempt to call a method from the superclass (SetOfPoints) from an instance of the subclass (LineSegment) work? Why do you think this is? Why does this make sense? Why is this useful?
Yes, because line Segements is a sub class of Set of points thus giving it all the poperties of the superclass.
This is useful because you dont have to continue to create the same methods over and over you can use those of the superclass

8. Were you allowed to create a SetOfPoints that was a Triangle—i.e., did the code successfully compile and run? Why do you think this is?
We downcasted the set of points to a triangle. this works because they have a relationship.

9. Did the downcasting and method calls function properly? Posit a reason for this behavior.


10. Did the downcasting and method calls function properly? Compare and contrast this with the situation examined in question 9. If the behavior differed, why do you think that is?

public class Runner{
	public static void main(String[] args){
		SetOfPoints set2;

		Point2D point1 = new Point2D(1.5, 2.5);
		Point2D point2 = new Point2D(4.0, 2.5);
		Point2D point3 = new Point2D(1.5, 2.5);
		Object point4 = (Object)point1;
		Point2D point5 = new Point2D(3.5,2.5);
		
		SetOfPoints set1 = new SetOfPoints(point1,point2,point3);
		LineSegment line  = new LineSegment(point1, point2);
		LineSegment line2 = new LineSegment();
		set2 = new Triangle();
		set2.add(point1);
		System.out.println("set2: " + set2.toString());
		set2.add(point2);
		set2.add(point5);
		set2 = (Triangle)set2;
		System.out.println("set2T: " + set2.toString());


		System.out.println("point1 toString :" +point1.toString());
		System.out.println("point2 toString :" +point2.toString());
		System.out.println("point3 toString :" +point3.toString());
		System.out.println("point4 toString :" +point4.toString());
		System.out.println("set1 :" + set1.toString()); 
		System.out.println("line :" + line.toString()); 
		line2.add(point1);
		System.out.println("line2 :" + line2.toString());



		System.out.println("point1 equals point2 :" +point1.equals(point2));
		System.out.println("point1 equals point3 :" +point1.equals(point3));
		
	
	}
}
public interface Moveable{
	//moves objects vertically
	void moveVertical(double distance);

	//moves object horizontally
	void moveHorizontal(double distance);
}
/*
*
*
*
*
*
*/

public class Runner{
	public static void main(String [] args){
		//initialize three vertex for the triangle
		Point2D triVert1 = new Point2D(0.0,0.0);
		Point2D triVert2 = new Point2D(0.0,3.0);
		Point2D triVert3 = new Point2D(4.0,3.0);

		//initialize a center point for the circle
		Point2D circleCenter = new Point2D(5.5,5.5);

		//create the circle and triangle
		Triangle myTri = new Triangle(triVert1,triVert2,triVert3);
		Circle myCircle = new Circle(circleCenter, 2.0);

		//array of shapes
		Shape[] shapes = new Shape[2];
		shapes[0] = myCircle;
		shapes[1] = myTri;
		
		for (Shape shape : shapes){
			System.out.println(shape +" has area " + shape.area());
			System.out.println();
		}

		//move shapes
		for(Shape shape : shapes){
			shape.moveVertical(2.0);
			shape.moveHorizontal(3.0);
		}
		
		for (Shape shape : shapes){
			System.out.println(shape +" has area " + shape.area());
			System.out.println();
		}

	}//end main method
}//end class runner